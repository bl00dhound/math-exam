import React, { useState, useCallback, useRef } from 'react';
import './App.css';
import Question from './components/Question';

const QUESTIONS = 30;
const FROM = 2;
const TO = 8;

const generateQuestion = () => ({
  first: Math.floor(Math.random() * (TO - FROM + 1) + FROM),
  second: Math.floor(Math.random() * (TO - FROM + 1) + FROM) 
});

const App = () => {
  const [correctAnswers, updateCorrectAnswers] = useState(0);
  const [wrongAnswers, updateWrongAnswers] = useState(0);
  const [left, updateLeft] = useState(QUESTIONS);
  const [questions, updateQuestions] = useState([generateQuestion()])
  const endRef = useRef();


  const onSubmit = useCallback((isCorrect) => {
    if (isCorrect) {
      updateCorrectAnswers(correctAnswers + 1);
    } else {
      updateWrongAnswers(wrongAnswers + 1);
    }
    if (left < 2) {
      alert('Екзамен закінчився.');
    } else {
      updateQuestions([...questions, generateQuestion()]);
      endRef?.current?.scrollIntoView({
        behavior: 'smooth',
        block: 'end',
        inline: 'nearest'
      });
    }

    updateLeft(left - 1);
  }, [correctAnswers, left, questions, wrongAnswers]);

  return (
    <div className="App">
      <h2 className="title">Надай правильні відповіді на всі запитання</h2>
      <div className="answers">
        <h4 className="correctAmount">Правильні відповіді: {correctAnswers}</h4>
        <h4 className="questionsAmount">Залишилось: {left}</h4>
        <h4 className="wrongAmount">Неправильні відповіді: {wrongAnswers}</h4>
      </div>
      {questions.map(({ first, second }, idx) => <Question
        key={idx}
        idx={idx}
        firstNumber={first}
        secondNumber={second}
        submit={onSubmit}
      />)}
      <div ref={endRef}></div>
    </div>
  );
}

export default App;
