import React, { useState, useCallback, useEffect } from 'react';
import './styles.css';
import { Button, Input } from 'semantic-ui-react';

const TIMEOUT = 8;

const Question = ({ firstNumber, secondNumber, submit, idx }) => {
  const [answer, updateAnswer] = useState('');
  const [tempAnswer, updateTempAnswer] = useState('');
  const [seconds, updateSeconds] = useState(TIMEOUT);


  useEffect(() => {
    let interval = setInterval(() => updateSeconds(() => seconds - 1), 1000)
    if (seconds < 1) {
      clearInterval(interval);
      if (!answer) {
        updateAnswer('Не встиг');
        submit(false);
      }
    }
    return () => {
      clearInterval(interval);
    }
  }, [seconds])


  const onChange = useCallback(event => {
    updateTempAnswer(event.target.value);
  }, []);

  const onClick = useCallback(() => {
    if (tempAnswer === '') return;
    updateAnswer(tempAnswer);
    submit(firstNumber * secondNumber === Number(tempAnswer));
    // clearInterval(interval);
  }, [firstNumber, secondNumber, submit, tempAnswer]);

  const enterKeyPress = event => {
    if (event.key === 'Enter') {
      onClick();
    }
  }

  return (
    <>
    <div className="questionWrapper">
      <div className="idx">{idx + 1}.</div>
        <div className="expression">
          <div className="firstNumber">{firstNumber}</div>
          <div className="multiplier">x</div>
          <div className="secondNumber">{secondNumber}</div>
          <div className="equal">=</div>
        </div>
      <Input autoFocus className="answer" disabled={Boolean(answer)} onChange={onChange} onKeyPress={enterKeyPress} />
        {answer !== ''
          ? (<div className={`result ${firstNumber * secondNumber === Number(answer) ? 'correct' : 'incorrect'}`}>{answer}</div>)
          : (<Button positive onClick={onClick}>Відповісти!</Button>)}
      
        <div className="timer">{seconds}s</div>
    </div>
  </>
  );
}

export default Question;
